package anselmoeana.matematicadivertida;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.util.ArrayList;
import java.util.Random;


import anselmoeana.matematicadivertida.helpers.Resultado;
import anselmoeana.matematicadivertida.helpers.Util;

public class MatematicaDivertidaMain extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matematica_divertida_main);
        Util.rand = new Random();
    }

    public void iniciarJogoContagem(View view) {
        Intent intent = new Intent(this, ContagemMain.class);
        ContagemStorage.jogoItens =  ContagemStorage.sorteia(5);
        ContagemStorage.resultado = new Resultado(5);
        intent.putExtra("questao", 0);
        intent.putExtra("questoes", 5);
        startActivity(intent);
    }

    public void iniciarJogoAritmetica(View view) {
        Intent intent = new Intent(this, AritmeticaMain.class);
        AritmeticaStorage.resultado = new Resultado(5);
        AritmeticaStorage.itens = new ArrayList<>();
        AritmeticaStorage.sorteia(5);
        intent.putExtra("questao", 0);
        intent.putExtra("questoes", 5);
        startActivity(intent);
    }

    public void iniciarJogoMaiorNumero(View view) {
        Intent intent = new Intent(this, MaiorNumeroMain.class);
        MaiorNumeroStorage.resultado = new Resultado(5);
        MaiorNumeroStorage.itens = new ArrayList<>();
        MaiorNumeroStorage.sorteia(5);
        intent.putExtra("questao", 0);
        intent.putExtra("questoes", 5);
        startActivity(intent);
    }


}
