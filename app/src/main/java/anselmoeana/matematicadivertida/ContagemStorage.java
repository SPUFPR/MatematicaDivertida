/**
 * Armazenamento de dados para o jogo Contagem
 */

package anselmoeana.matematicadivertida;


import java.util.HashSet;
import java.util.Set;

import anselmoeana.matematicadivertida.helpers.Resultado;
import anselmoeana.matematicadivertida.helpers.Util;

public class ContagemStorage {
    /**
     * Itens existentes (com imagem)
     */
    public static ContagemItem[] contagemItens = {
            new ContagemItem(
                    R.drawable.contagem_macas,
                    "Quantas maçãs você vê na foto?",
                    2,
                    new int[]{2, 3, 5}
            ),
            new ContagemItem(
                    R.drawable.contagem_carros,
                    "Quantos carros você vê na foto?",
                    7,
                    new int[]{5, 7, 9}
            ),
            new ContagemItem(
                    R.drawable.contagem_caquis,
                    "Quantos caquis você vê na foto?",
                    3,
                    new int[]{1, 2, 3}
            ),
            new ContagemItem(
                    R.drawable.contagem_cadeiras2,
                    "Quantas cadeiras você vê na foto?",
                    8,
                    new int[]{8, 10, 11}
            ),
            new ContagemItem(
                    R.drawable.contagem_cadeiras,
                    "Quantas cadeiras você vê na foto?",
                    4,
                    new int[]{2, 3, 4}
            ),
            new ContagemItem(
                    R.drawable.contagem_gatinhos,
                    "Quantos gatinhos você vê na foto?",
                    5,
                    new int[]{5, 7, 9}
            ),
            new ContagemItem(
                    R.drawable.contagem_gatinhos2,
                    "Quantos gatinhos você vê na foto?",
                    3,
                    new int[]{0, 3, 7}
            ),
            new ContagemItem(
                    R.drawable.contagem_papagaios,
                    "Quantos pássaros você vê na foto?",
                    3,
                    new int[]{3, 6, 7}
            ),
            new ContagemItem(
                    R.drawable.contagem_peixes,
                    "Quantos peixes você vê na foto?",
                    2,
                    new int[]{0, 2, 4}
            ),
            new ContagemItem(
                    R.drawable.contagem_pessoas,
                    "Quantas pessoas você vê na foto?",
                    9,
                    new int[]{5, 7, 9}
            )
    };

    /**
     * Resultados do jogo ativo
     */
    public static Resultado resultado;
    /**
     * Itens do jogo ativo
     */
    public static ContagemItem[] jogoItens;

    /**
     * Faz o sorteio das questões (itens) para novo jogo
     *
     * @param questoes
     * @return
     */
    public static ContagemItem[] sorteia(int questoes) {
        ContagemItem[] items = new ContagemItem[questoes];
        Set<Integer> sorted = new HashSet<>();
        for (int i = 0; i < questoes; ++i) {
            while (true) {
                Integer generated = Util.rand.nextInt(contagemItens.length - 1);
                if (!sorted.contains(generated)) {
                    sorted.add(generated);
                    items[i] = contagemItens[generated];
                    break;
                }
            }
        }
        return items;
    }
}
