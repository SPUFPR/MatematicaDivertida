/**
 * Armazenamento de dados para o jogo Aritmética
 */

package anselmoeana.matematicadivertida;

import java.util.List;

import anselmoeana.matematicadivertida.helpers.Resultado;

public class AritmeticaStorage {
    /**
     * Resultados do jogo ativo
     */
    public static Resultado resultado;
    /**
     * Itens (expressões) do jogo ativo
     */
    public static List<AritmeticaItem> itens;

    /**
     * Gera nova questão (item)
     *
     * @return
     */
    public static void gerarQuestao() {
        while (true) {
            AritmeticaItem item = new AritmeticaItem();
            if (!itens.contains(item)) {
                itens.add(item);
                break;
            }
        }
    }

    /**
     * Sorteia (gera) questoes para uma sessão do jogo
     *
     * @param questoes
     */
    public static void sorteia(int questoes) {
        for (int i = 0; i < questoes; ++i)
            gerarQuestao();
    }
}
