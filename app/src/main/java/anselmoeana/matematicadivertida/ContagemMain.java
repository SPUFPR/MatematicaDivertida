/**
 * Controller da tela do jogo Contagem
 */
package anselmoeana.matematicadivertida;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ContagemMain extends AppCompatActivity {
    /**
     * Variáveis passadas de uma instância para outra
     */
    private int qindex, qtotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(null);
        setContentView(R.layout.activity_contagem_main);
        Intent intent = getIntent();
        TextView titulo = findViewById(R.id.questaoTituto);
        ImageView img = findViewById(R.id.contagemImagem);
        TextView questao = findViewById(R.id.questaoText);
        Button r1 = findViewById(R.id.resposta1);
        Button r2 = findViewById(R.id.resposta2);
        Button r3 = findViewById(R.id.resposta3);
        qindex = intent.getIntExtra("questao", 0);
        qtotal = intent.getIntExtra("questoes", 0);
        titulo.setText(String.format("Questão %d/%d", qindex + 1, qtotal));
        questao.setText(ContagemStorage.jogoItens[qindex].getQuestion());
        r1.setText(String.format("%d", ContagemStorage.jogoItens[qindex].getOptions()[0]));
        r2.setText(String.format("%d", ContagemStorage.jogoItens[qindex].getOptions()[1]));
        r3.setText(String.format("%d", ContagemStorage.jogoItens[qindex].getOptions()[2]));
        img.setImageResource(ContagemStorage.jogoItens[qindex].getResource());
    }

    /**
     * Verifica a resposta do usuário, enviando para a próxima tela
     *
     * @param view
     */
    public void resposta(View view) {
        Button btn = (Button) view;
        int answer = -1;
        ContagemItem item = ContagemStorage.jogoItens[qindex];
        switch (btn.getId()) {
            case R.id.resposta1:
                answer = item.getOptions()[0];
                break;
            case R.id.resposta2:
                answer = item.getOptions()[1];
                break;
            case R.id.resposta3:
                answer = item.getOptions()[2];
                break;
        }
        System.err.println(answer);
        System.err.println(item.getAnswer());
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        if (item.checkAnswer(answer)) {
            alert.setTitle("Resposta correta!");
            ContagemStorage.resultado.setResposta(qindex, true);
        } else {
            alert
                    .setTitle("Resposta errada!")
                    .setMessage(String.format("A resposta correta é: %d (sua resposta foi: %d)", item.getAnswer(), answer));
            ContagemStorage.resultado.setResposta(qindex, false);
        }
        String botaoText = "Próxima questão";
        if (qindex >= (qtotal - 1)) {
            botaoText = "Ver resultados";
        }
        alert
                .setPositiveButton(botaoText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent;
                        if (qindex >= (qtotal - 1)) {
                            intent = new Intent(ContagemMain.this, ResultadoMain.class);
                            intent.putExtra("acertos", ContagemStorage.resultado.getAcertos());
                            intent.putExtra("questoes", qtotal);
                        } else {
                            intent = new Intent(ContagemMain.this, ContagemMain.class);
                            intent.putExtra("questao", qindex + 1);
                            intent.putExtra("questoes", qtotal);
                        }
                        startActivity(intent);
                        finish();
                    }
                })
                .show();

    }
}
