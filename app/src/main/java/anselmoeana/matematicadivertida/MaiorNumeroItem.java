/**
 * Item para o jogo Maior Número
 */
package anselmoeana.matematicadivertida;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import anselmoeana.matematicadivertida.helpers.Util;


public class MaiorNumeroItem {
    /**
     * Número de dígitos
     */
    private int numDigits;
    /**
     * Resposta correta (caculada na construção)
     */
    private int answer;
    /**
     * Dígitos gerados
     */
    private List<Integer> digits;
    /**
     * Construtor
     */
    public MaiorNumeroItem() {
        this(3);
    }

    public MaiorNumeroItem(Integer numDigits) {
        this.numDigits = numDigits;
        digits = new ArrayList<>();
        generate();
    }

    /**
     * Gera dígitos
     */
    public void generate() {
        for (int i = 0; i < numDigits; ++i) {
            digits.add(Util.getRandRange(0, 9));
        }
        Collections.shuffle(digits);
        List<Integer> digitsCopy = new ArrayList<>(digits);
        Collections.sort(digitsCopy, Collections.<Integer>reverseOrder());
        answer = calculate(digitsCopy);
    }

    /**
     * Computa o valor numérico dos dígitos informados, lendo-se da esquerda para a direita
     * @param dgts
     * @return
     */
    private int calculate(List<Integer> dgts) {
        int result = 0;
        Integer exp = dgts.size() - 1;
        for (int i = 0; i < dgts.size() && exp > -1; ++i, --exp) {
            result += (dgts.get(i) * (new Double(Math.pow(10, exp))).intValue());
        }
        return result;
    }

    /**
     * Retorna os dígitos gerados
     * @return
     */
    public List<Integer> getDigits() {
        return digits;
    }

    /**
     * Verifica se a resposta está correta
     */
    public boolean checkAnswer(int answer) {
        return (answer == this.answer);
    }

    public int getAnswer() {
        return answer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MaiorNumeroItem)) return false;
        MaiorNumeroItem that = (MaiorNumeroItem) o;
        /**
         *  Comparação das listas
         */
        if (numDigits == that.numDigits) {
            List<Integer> myDigits = new ArrayList<>(digits);
            List<Integer> itsDigits = new ArrayList<>(that.digits);
            Collections.sort(myDigits);
            Collections.sort(itsDigits);
            Integer[] myDigitsArr = myDigits.toArray(new Integer[0]);
            Integer[] itsDigitsArr = itsDigits.toArray(new Integer[0]);
            boolean areEqual = true;
            for (int i = 0; i < myDigitsArr.length; ++i) {
                if (myDigitsArr[i] != itsDigitsArr[i]) {
                    areEqual = false;
                }
            }
            return areEqual;
        }
        return false;
    }


}