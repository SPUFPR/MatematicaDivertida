/**
 * Controller da tela do jogo Maior Número
 */
package anselmoeana.matematicadivertida;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MaiorNumeroMain extends AppCompatActivity {

    private int qindex, qtotal;
    private MaiorNumeroItem item;
    private int[] options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maior_numero_main);
        Intent intent = getIntent();
        TextView titulo = findViewById(R.id.questaoTituto);
        EditText d1 = findViewById(R.id.digito1);
        EditText d2 = findViewById(R.id.digito2);
        EditText d3 = findViewById(R.id.digito3);
        qindex = intent.getIntExtra("questao", 0);
        qtotal = intent.getIntExtra("questoes", 0);
        titulo.setText(String.format("Questão %d/%d", qindex + 1, qtotal));
        item = MaiorNumeroStorage.itens.get(qindex);
        d1.setText(String.format("%d", item.getDigits().get(0)));
        d2.setText(String.format("%d", item.getDigits().get(1)));
        d3.setText(String.format("%d", item.getDigits().get(2)));
    }

    /**
     * Verifica a resposta do usuário, enviando para a próxima tela
     *
     * @param view
     */
    public void resposta(View view) {
        Button btn = (Button) view;
        int answer = -1;
        boolean answered = true;
        EditText resp = findViewById(R.id.resposta);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        if(resp.getText().toString().length() < 1) {
            answered = false;
        } else {
            answer = Integer.parseInt(resp.getText().toString());
            if (item.checkAnswer(answer)) {
                alert.setTitle("Resposta correta!");
                MaiorNumeroStorage.resultado.setResposta(qindex, true);
            } else {
                alert
                        .setTitle("Resposta errada!")
                        .setMessage(String.format("A resposta correta é: %d (sua resposta foi: %d)", item.getAnswer(), answer));
                MaiorNumeroStorage.resultado.setResposta(qindex, false);
            }
        }
        String botaoText = "Próxima questão";
        if (qindex >= (qtotal - 1)) {
            botaoText = "Ver resultados";
        }
        if(answered) {
            alert
                    .setPositiveButton(botaoText, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent intent;
                            if (qindex >= (qtotal - 1)) {
                                intent = new Intent(MaiorNumeroMain.this, ResultadoMain.class);
                                intent.putExtra("acertos", MaiorNumeroStorage.resultado.getAcertos());
                                intent.putExtra("questoes", qtotal);
                            } else {
                                intent = new Intent(MaiorNumeroMain.this, MaiorNumeroMain.class);
                                intent.putExtra("questao", qindex + 1);
                                intent.putExtra("questoes", qtotal);
                            }
                            startActivity(intent);
                            finish();
                        }
                    })
                    .show();
        } else {
            alert.setMessage("Informe uma Resposta!").setPositiveButton("Voltar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();
        }
    }
}
