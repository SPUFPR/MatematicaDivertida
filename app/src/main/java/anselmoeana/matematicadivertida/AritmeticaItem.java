/**
 * Item para o jogo Aritmética
 */
package anselmoeana.matematicadivertida;

import java.util.Objects;

import anselmoeana.matematicadivertida.helpers.Util;

public class AritmeticaItem {
    /**
     * Operando A
     */
    private int operandA;
    /**
     * Operando B
     */
    private int operandB;
    /**
     * Operador da expressão
     */
    private char operator;
    /**
     * Universo de operadores para sorteio (geração automática de expressão)
     */
    private char[] operators = {'-', '+', '-', '+', '-', '+', '-', '+', '-', '+', '-', '+', '-', '+', '-', '+', '-', '+', '-', '+', '-', '+'};

    /**
     * Construtor
     */
    public AritmeticaItem() {
        generate();
    }

    /**
     * Gera operandos e operador (expressao)
     */
    public void generate() {
        operandA = Util.getRandRange(1, 10);
        if (operandA > 8) {
            operandB = Util.getRandRange(0, operandA - 1);
        } else {
            operandB = Util.getRandRange(operandA + 1, 10);
        }
        operator = operators[Util.getRandRange(0, operators.length - 1)];
        checkOperands();
    }

    /**
     * Para o resultado não ser negativo
     */
    private void checkOperands() {
        if (operator == '-') {
            if (operandA < operandB) {
                int t = operandA;
                operandA = operandB;
                operandB = t;
            }
        }
    }

    public int getOperandA() {
        return operandA;
    }

    public void setOperandA(int operandA) {
        this.operandA = operandA;
    }

    public int getOperandB() {
        return operandB;
    }

    public void setOperandB(int operandB) {
        this.operandB = operandB;
    }

    public char getOperator() {
        return operator;
    }

    public void setOperator(char operator) {
        this.operator = operator;
    }

    /**
     * Retorna expressão em texto (notação infixa)
     *
     * @return
     */
    public String getExpression() {
        return String.format("%d %c %d", operandA, operator, operandB);
    }

    /**
     * Verifica se a resposta da expressão é igual a fornecida
     *
     * @param answer
     * @return
     */
    public boolean checkAnswer(int answer) {
        if (answer == calculate()) {
            return true;
        }
        return false;
    }

    /**
     * Calcula a resposta
     *
     * @return
     */
    public int calculate() {
        int result = 0;
        switch (operator) {
            case '+':
                result = operandA + operandB;
                break;
            case '-':
                result = operandA - operandB;
                break;
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AritmeticaItem)) return false;
        AritmeticaItem that = (AritmeticaItem) o;
        return operandA == that.operandA &&
                operandB == that.operandB &&
                operator == that.operator;
    }

    @Override
    public int hashCode() {

        return Objects.hash(operandA, operandB, operator);
    }
}
