/**
 * Item para o jogo Contagem
 */
package anselmoeana.matematicadivertida;

public class ContagemItem {
    /**
     * Resource da imagem
     */
    private int resource;
    /**
     * Questão a exibir na tela
     */
    private String question;
    /**
     * Opções de resposta (botões)
     */
    private int[] options;
    /**
     * Resposta correta
     */
    private int answer;

    /**
     * Construtor
     *
     * @param resource
     * @param question
     * @param answer
     * @param options
     */
    public ContagemItem(int resource, String question, int answer, int[] options) {
        this.resource = resource;
        this.question = question;
        this.answer = answer;
        this.options = options;
    }

    public int getResource() {
        return resource;
    }

    public void setResource(int resource) {
        this.resource = resource;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int[] getOptions() {
        return options;
    }

    public void setOptions(int[] options) {
        this.options = options;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    /**
     * Verifica se a resposta fornecida está correta
     * @param answer
     * @return
     */
    public boolean checkAnswer(int answer) {
        if (this.answer == answer) {
            return true;
        }
        return false;
    }
}
