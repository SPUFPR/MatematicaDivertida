/**
 * Armazenamento de dados para o jogo Maior Número
 */
package anselmoeana.matematicadivertida;

import java.util.List;

import anselmoeana.matematicadivertida.helpers.Resultado;


public class MaiorNumeroStorage {
    /**
     * Resultados do jogo ativo
     */
    public static Resultado resultado;
    /**
     * Itens do jogo ativo
     */
    public static List<MaiorNumeroItem> itens;

    /**
     * Gera nova questão/item
     *
     * @return
     */
    public static void gerarQuestao() {
        while (true) {
            MaiorNumeroItem item = new MaiorNumeroItem();
            if (!itens.contains(item)) {
                itens.add(item);
                break;
            }
        }
    }

    /**
     * Sorteia (gera) questoes para uma sessão do jogo
     *
     * @param questoes
     */
    public static void sorteia(int questoes) {
        for(int i = 0; i < questoes; ++i)
            gerarQuestao();
    }

}
