/**
 * Controller da Tela do jogo Artimética
 */
package anselmoeana.matematicadivertida;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class AritmeticaMain extends AppCompatActivity {
    /**
     * Variáveis passadas entre as instâncias (telas)
     */
    private int qindex, qtotal;
    /**
     * Item gerado para esta tela
     */
    private AritmeticaItem item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aritmetica_main);
        Intent intent = getIntent();
        TextView titulo = findViewById(R.id.questaoTituto);
        TextView expressao = findViewById(R.id.expressaoText);
        qindex = intent.getIntExtra("questao", 0);
        qtotal = intent.getIntExtra("questoes", 0);
        titulo.setText(String.format("Questão %d/%d", qindex + 1, qtotal));
        item = AritmeticaStorage.itens.get(qindex);
        expressao.setText(item.getExpression());
    }

    /**
     * Verifica a resposta do usuário, enviando para a próxima tela
     *
     * @param view
     */
    public void resposta(View view) {
        EditText resp = findViewById(R.id.resposta);
        int answer = -1;
        boolean answered = true;
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        if(resp.getText().toString().length() < 1) {
            answered = false;
        } else {
            answer = Integer.parseInt(resp.getText().toString());
            if (item.checkAnswer(answer)) {
                alert.setTitle("Resposta correta!");
                AritmeticaStorage.resultado.setResposta(qindex, true);
            } else {
                alert
                        .setTitle("Resposta errada!")
                        .setMessage(String.format("A resposta correta é: %d (sua resposta foi: %d)", item.calculate(), answer));
                AritmeticaStorage.resultado.setResposta(qindex, false);
            }
        }
        String botaoText = "Próxima questão";
        if (qindex >= (qtotal - 1)) {
            botaoText = "Ver resultados";
        }
        if(answered) {
            alert
                    .setPositiveButton(botaoText, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent intent;
                            if (qindex >= (qtotal - 1)) {
                                intent = new Intent(AritmeticaMain.this, ResultadoMain.class);
                                intent.putExtra("acertos", AritmeticaStorage.resultado.getAcertos());
                                intent.putExtra("questoes", qtotal);
                            } else {
                                intent = new Intent(AritmeticaMain.this, AritmeticaMain.class);
                                intent.putExtra("questao", qindex + 1);
                                intent.putExtra("questoes", qtotal);
                            }
                            startActivity(intent);
                            finish();
                        }
                    })
                    .show();
        } else {
            alert.setMessage("Informe uma Resposta!").setPositiveButton("Voltar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();
        }
    }
}
