package anselmoeana.matematicadivertida;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


public class ResultadoMain extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado_main);
        Intent intent = getIntent();
        Integer acertos = intent.getIntExtra("acertos", 0);
        Integer questoes = intent.getIntExtra("questoes", 0);
        float nota = acertos.floatValue() / questoes.floatValue() * 100;

        TextView acertoText = findViewById(R.id.acertoText);
        acertoText.setText(String.format("Sua nota é %.0f (%d/%d questões)", nota, acertos, questoes));
    }

    public void inicio(View view) {
        finish();
    }
}
