/**
 * Utilidades
 */
package anselmoeana.matematicadivertida.helpers;

import java.util.Random;

public class Util {
    /**
     * Gerador de números aleatórios
     */
    public static Random rand;
    /**
     * Último número gerado (para evitar repetições consecutivas)
     */
    private static int last = 0;

    /**
     * Retorna um número aleatório entre min e max (inclusive)
     *
     * @param min
     * @param max
     * @return
     */
    public static int getRandRange(int min, int max) {
        int result = 0;
        while (true) {
            try {
                if (max == min && (min == 0 || (max - min) == 0))
                    return 1;
                else
                    result = (rand.nextInt(100) % (max - min + 1)) + min;
                if (result != last) {
                    last = result;
                    break;
                }
            } catch (Exception ex) {
            }
        }
        return result;
    }

    /**
     * Fatorial (para pequenos números)
     *
     * @param num
     * @return
     */
    public static int factorial(int num) {
        if (num > 10)
            return -1;
        if (num < 2)
            return 1;
        int result = 1;
        for (int i = 2; i <= num; ++i)
            result *= i;
        return result;
    }
}
