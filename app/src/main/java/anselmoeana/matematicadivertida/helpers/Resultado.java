/**
 * Armazena os resutados (respostas) de cada iteração de um jogo
 */
package anselmoeana.matematicadivertida.helpers;

import java.util.ArrayList;
import java.util.List;

public class Resultado {
    /**
     * Número de questões armazenadas
     */
    private int questoes;
    /**
     * Armazena acerto(true)/erro(false) para cada questão
     */
    private List<Boolean> respostas;

    /**
     * Construtor
     *
     * @param questoes número de questões
     */
    public Resultado(int questoes) {
        this.questoes = questoes;
        this.respostas = new ArrayList<>();
        for (int i = 0; i < questoes; ++i) {
            this.respostas.add(false);
        }
    }

    /**
     * Informa situação de uma questão (true/false)
     *
     * @param questao
     * @param valor
     */
    public void setResposta(int questao, boolean valor) {
        this.respostas.set(questao, valor);
    }

    /**
     * Retorna o número de questões deste resultado
     *
     * @return
     */
    public int getQuestoes() {
        return this.questoes;
    }

    /**
     * Computa o número de acertos
     *
     * @return
     */
    public int getAcertos() {
        int acertos = 0;
        for (Boolean v : respostas) {
            if (v) {
                ++acertos;
            }
        }
        return acertos;
    }


}
